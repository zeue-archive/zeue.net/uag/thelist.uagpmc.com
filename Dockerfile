FROM composer:latest

COPY . ./thelist.uagpmc.com

RUN cd thelist.uagpmc.com && composer install

EXPOSE 80/tcp

CMD cd thelist.uagpmc.com && php -S 0.0.0.0:80 -t ./src
