<?php

  require "../vendor/autoload.php";
  use Symfony\Component\Yaml\Yaml;
  $v = Yaml::parseFile("../inc/list.yaml");

?>

<style media="screen">
  @import url('https://fonts.googleapis.com/css?family=Black+Ops+One');

  * {
    font-family: 'Black Ops One', cursive;
  }
</style>

<script type="text/javascript" src="list.js"></script>

<div id="rankings">

  <input class="search" placeholder="Search" />
  <b>Sort by:</b>
  <button class="sort" data-sort="name">
    Name
  </button>
  <button class="sort" data-sort="position">
    Rank
  </button>

  <ul class="list" style="list-style-type:none">
    <?php for ($i=0; $i < count($v); $i++) { ?>
      <li>
        <span class="position" style="font-weight:bold;">
          <?php echo ($i+1)." - "; ?>
        </span>
        <span class="name">
          <?php echo $v[$i]; ?>
        </span>
      </li>
    <?php } ?>
  </ul>

</div>

<script type="text/javascript">
  var options = {
    valueNames: [ 'position', 'name' ]
  };

  var userList = new List('rankings', options);
</script>
